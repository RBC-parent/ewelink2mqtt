FROM python:3.6.5-slim-stretch

RUN apt-get update \
&& apt-get install gcc -y \
&& apt-get clean

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN MULTIDICT_NO_EXTENSIONS=1 pip install --no-cache-dir -r requirements.txt

COPY . .

# CMD [ "python", "./collector.py" ]

CMD ./ewelink_server.py